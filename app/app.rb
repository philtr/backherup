require 'yaml'

module BackHerUp
  CONFIG = Hashie::Mash.new(YAML::load(File.read('config/backherup.yml')) || {})

  class App < Sinatra::Base
    configure do
      set :views, File.dirname(__FILE__) + '/views'
    end

    get '/' do
      "B redundant!"
    end

    get '/:app_name/backup' do
      @app_name = params[:app_name]
      @output = Task.backup(@app_name)
      erb :backup
    end
  end

  class Task
    class << self

      def backup(app_name)
        output = "Starting backup process...\n"
        output << download_source(app_name)
        output << capture_database(app_name)
        output << download_s3_files(app_name)
        output << make_archive(app_name)
        output << upload_to_s3(app_name)
        output << clean_tmpdir
        output << "\n\nWebsite backup complete.\n"
      end

    protected

      def sys(commands)
        `#{[commands].flatten.join(' && ')}`
      end

      def hcmd(app_name, command)
        "bundle exec heroku #{command} --app #{app_name}"
      end

      def download_source(app_name)
        op = "(Re)downloading source code from Heroku...\n"
        commands = [
          "rm -rf tmp/#{app_name}",
          "git clone --depth=1 git@heroku.com:#{app_name}.git tmp/#{app_name}",
          "cd tmp/#{app_name}",
          "rm -rf .git" ]
        op << sys(commands)
      end

      def capture_database(app_name)
        op = "Deleting oldest db backup and capturing current database...\n"
        op << delete_oldest_db_backup(app_name)
        commands = [
          hcmd(app_name, "pgbackups:capture"),
          "mkdir -p tmp/#{app_name}/db",
          "curl -o tmp/#{app_name}/db/#{app_name}.pgsql.dump `#{ hcmd app_name, "pgbackups:url" }`" ]
        op << sys(commands)
      end

      def delete_oldest_db_backup(app_name)
        backups = sys(hcmd(app_name, "pgbackups")).
                  split("\n").
                  grep(/^b\d{3}/) do |line|
                    line.match(/^b\d{3}/).
                    to_s.sub('b','').to_i
                  end
        op = sys(hcmd(app_name, "pgbackups:destroy #{"b%03d" % backups.sort.first}")) unless backups.empty?
      end

      def download_s3_files(app_name)
        download_bucket = (CONFIG.aws.buckets[app_name] || app_name)
        output_dir = "tmp/#{app_name}/s3"
        output = "Downloading files from S3 bucket #{download_bucket}...\n"
        AWS::S3::Base.establish_connection!({
          :access_key_id => CONFIG.aws.access_key_id,
          :secret_access_key => CONFIG.aws.secret_access_key
        })
        app_bucket = AWS::S3::Bucket.find(download_bucket)
        app_bucket.objects.each do |object|
          output << "Downloading #{object.key}..."
          sys "mkdir -p #{output_dir}/#{object.key.split('/').slice(0..-2).join('/')}"
          begin
            open("#{output_dir}/#{object.key}", 'w:ASCII-8BIT') do |file|
              AWS::S3::S3Object.stream(object.key, download_bucket) do |chunk|
                file.write chunk
              end
            end
          rescue
            output << "ERROR!\n"
          else
            output << "done.\n"
          end
        end
        AWS::S3::Base.disconnect!
        output << "Files successfully downloaded from S3 bucket #{download_bucket}.\n\n"
      end

      def make_archive(app_name)
        output = "Compressing files..."
        commands = [
          "cd tmp/",
          "tar -czf #{app_name}.#{Time.now.strftime("%w.%A")}.tar.gz #{app_name}" ]
        sys commands
        output << "done.\n"
      end

      def upload_to_s3(app_name)
        output = "Uploading backup file to S3..."
        AWS::S3::Base.establish_connection!({
          :access_key_id => CONFIG.aws.access_key_id,
          :secret_access_key => CONFIG.aws.secret_access_key
        })
        backup_file = Dir.glob("tmp/#{app_name}.*.gz").last
        AWS::S3::S3Object.store(backup_file.sub(/^tmp/,app_name), open(backup_file), CONFIG.aws.buckets.backup)
        output << "done.\n"
      end

      def clean_tmpdir
        output = "Cleaning tmp/ directory..."
        sys('rm -rf tmp/*')
        output << "done.\n"
      end
    end
  end
end
