require 'rubygems'
require 'bundler'
Bundler.require(:default, :framework, :libraries)

require File.join(File.dirname(__FILE__),'app/app')
run BackHerUp::App
